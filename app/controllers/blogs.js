var db = require('../../config/db');
// var firebase = require('../../config/firebase');

const saltRounds = 10;

function Blogs() {
  
  var _this = this;

  this.createArticle = function(req, res) {
    console.log("create an article");
    db.acquire(function(err, con) {
      let query = 'INSERT INTO articles (nickname, title, content, created_at, updated_at) VALUES (?, ?, ?, NOW(), NOW())';
      con.query(query, [req.body.nickname, req.body.title, req.body.content], function(err, data) {
      con.release();
        if (err) {
          return res.json({status:400, message: 'Failed, connection error', result:err});
        } else {
          return res.json({status:200, message: 'Successfully created an article', result:data});
        }
      });
    });
  };

  this.getArticles = function(req, res) {
    console.log("get all articles");
    db.acquire(function(err, con) {
      let query = 'SELECT * FROM articles ORDER BY created_at DESC LIMIT ?, 20';
      con.query(query, [req.body.offset], function(err, data) {
      con.release();
        if (err) {
          return res.json({status:400, message: 'Failed, connection error', result:err});
        } else {
          return res.json({status:200, message: 'Successfully get all articles', result:data});
        }
      });
    });
  };  

  this.findArticle = function(req, res) {
    console.log("find an article");
    db.acquire(function(err, con) {
      let query = 'SELECT * FROM articles WHERE id = ?';
      con.query(query, [req.body.article_id], function(err, data) {
      con.release();
        if (err) {
          return res.json({status:400, message: 'Failed, connection error', result:err});
        } else {
          return res.json({status:200, message: 'Successfully get article detail', result:data[0]});
        }
      });
    });
  };  

  this.createComment = function(req, res) {
    console.log("create an comment");
    if(req.body.type == 1){
      db.acquire(function(err, con) {
        let query = 'SELECT * FROM articles \
                    WHERE id = ?';
        con.query(query, [req.body.commentable_id], function(err, article) {
          con.release();
          if (err) {
            return res.json({status:400, message: 'Failed, connection error', result:err});
          } else {
            if(article.length > 0) {
              let query = 'INSERT INTO comments (commentable_id, nickname, text, type, created_at, updated_at) VALUES (?, ?, ?, ?, NOW(), NOW())';
              con.query(query, [req.body.commentable_id, req.body.nickname, req.body.text, req.body.type], function(err, data) {
                if (err) {
                  return res.json({status:400, message: 'Failed, connection error', result:err});
                } else {
                  return res.json({status:200, message: 'Successfully created an article', result:data});
                }
              });
            }
            else{
               return res.json({status:204, message: 'Article was not found', result:data});
            }
          }
        });
      });
    }
    else if(req.body.type == 2){
      db.acquire(function(err, con) {
        let query = 'SELECT * FROM comments \
                    WHERE id = ?';
        con.query(query, [req.body.commentable_id], function(err, comment) {
          con.release();
          if (err) {
            return res.json({status:400, message: 'Failed, connection error', result:err});
          } else {
            if(comment.length > 0) {
              let query = 'INSERT INTO comments (commentable_id, nickname, text, type, created_at, updated_at) VALUES (?, ?, ?, ?, NOW(), NOW())';
              con.query(query, [req.body.commentable_id, req.body.nickname, req.body.text, req.body.type], function(err, data) {
                if (err) {
                  return res.json({status:400, message: 'Failed, connection error', result:err});
                } else {
                  return res.json({status:200, message: 'Successfully created an article', result:data});
                }
              });
            }
            else{
              return res.json({status:204, message: 'Comment was not found', result:data});
            }
          }
        });
      });
    }
    else{
      return res.json({status:204, message: 'Type is invalid', result:data});
    }
  };

  this.getComments = function(req, res) {
    console.log("get all comments");
    if(req.body.type == 1){
      db.acquire(function(err, con) {
        let query = 'SELECT * FROM articles \
                    WHERE id = ?';
        con.query(query, [req.body.commentable_id], function(err, article) {
          con.release();
          if (err) {
            return res.json({status:400, message: 'Failed, connection error', result:err});
          } else {
            if(article.length > 0) {
              db.acquire(function(err, con) {
                let query = 'SELECT * FROM comments \
                            WHERE commentable_id = ? AND type = ? \
                            ORDER BY created_at DESC LIMIT ?, 10';
                con.query(query, [req.body.commentable_id, req.body.type, req.body.offset], function(err, comments) {
                  if (err) {
                    return res.json({status:400, message: 'Failed, connection error', result:err});
                  } else {
                    return res.json({status:200, message: 'Successfully get all comments', result:{commentable: article[0], comments: comments}});
                  }
                });
              });
            }
            else{
               return res.json({status:204, message: 'Article was not found', result:article});
            }
          }
        });
      });
    }
    else if(req.body.type == 2){
      db.acquire(function(err, con) {
        let query = 'SELECT * FROM comments \
                    WHERE id = ?';
        con.query(query, [req.body.commentable_id], function(err, comment) {
          con.release();
          if (err) {
            return res.json({status:400, message: 'Failed, connection error', result:err});
          } else {
            if(comment.length > 0) {
              db.acquire(function(err, con) {
                let query = 'SELECT * FROM comments \
                            WHERE commentable_id = ? AND type = ? \
                            ORDER BY created_at DESC LIMIT ?, 10';
                con.query(query, [req.body.commentable_id, req.body.type, req.body.offset], function(err, comments) {
                  if (err) {
                    return res.json({status:400, message: 'Failed, connection error', result:err});
                  } else {
                    return res.json({status:200, message: 'Successfully get all comments', result:{commentable: comment[0], comments: comments}});
                  }
                });
              });
            }
            else{
               return res.json({status:204, message: 'Comment was not found', result:comment});
            }
          }
        });
      });
    }
  };  

}

module.exports = new Blogs();


