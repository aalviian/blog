var blogs = require('../controllers/blogs');
 
module.exports = {
  configure: function(app) {
    app.route('/blogs/create-article').post(blogs.createArticle);
    app.route('/blogs/get-articles').post(blogs.getArticles);
    app.route('/blogs/find-article').post(blogs.findArticle);
    app.route('/blogs/create-comment').post(blogs.createComment);
    app.route('/blogs/get-comments').post(blogs.getComments);
  }
};
