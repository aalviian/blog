var admin = require("firebase-admin");

var serviceAccount = require("../config/blog-api-32cfb-firebase-adminsdk-ui6lj-a54c1de69d.json");


function Connection() {

  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://blog-api-32cfb.firebaseio.com"
  });

  // admin.database.enableLogging(true);

  var db = admin.database();

  db.ref('users/1').set({
    username: "Alvian",
    email: "aalviian@gmail.com"
  });

  db.ref('users/2').set({
    username: "Alvian2",
    email: "aalviian2@gmail.com"
  });

}
module.exports = new Connection();